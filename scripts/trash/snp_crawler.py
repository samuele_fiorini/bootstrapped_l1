import os
import numpy as np
import pandas as pd

import logging

def get_samples(path):
    """
    Recursively browse the input folder and return the full path for each csv file
    """
    samples_fileNames = []
    for elem in os.listdir(path):
        if os.path.isdir(os.path.join(path,elem)): # if it's a folder
            for file_ in os.listdir(os.path.join(path,elem)):
                if file_.endswith('.csv'):
                    samples_fileNames.append(os.path.join(path, elem, file_))
    return samples_fileNames

def get_ref_genome(fileName):
    """
    Get the reference genome from plink.frq.count file
    """
    refdata = pd.io.parsers.read_csv(fileName)

    SNPs = []
    ref = []
    alt = []
    count_ref = []
    count_alt = []
    for row in refdata.values:
        splitted_row = np.array(row[0].split(' '))
        pos =  np.argwhere(splitted_row!='') # [CHR SNP A1 A2 C1 C2 G0]
        SNPs.append(splitted_row[pos[1]][0])
        alt.append(splitted_row[pos[2]][0]) # A1
        ref.append(splitted_row[pos[3]][0]) # A2
        count_ref.append(splitted_row[pos[5]][0]) # C2
        count_alt.append(splitted_row[pos[4]][0]) # C1

    ref_genome = pd.DataFrame(data = np.vstack((ref, count_ref, alt, count_alt)).T,
                              index = SNPs,
                              columns = ['REF', 'COUNT_REF', 'ALT', 'COUNT_ALT'],
                              dtype = None)

    #ref_genome['COUNT_REF'] = ref_genome['COUNT_REF'].apply(int)
    #ref_genome['COUNT_ALT'] = ref_genome['COUNT_ALT'].apply(int)
    return ref_genome

def to012(gt_call, ref):
    """
    Encode the GT call in 0,1,2
    """
    gt = 0
    for g in gt_call:
        if g != ref:
            gt+=1
    return gt

def main():
    # Logging options
    LOG_FILENAME = 'logging_snp_crawler.out'
    logging.basicConfig(filename = LOG_FILENAME,
                        level = logging.DEBUG)


    # Get samples files
    path = "/home/samu/projects/Special_issue_ADNI/data"
    files = get_samples(path)
    n_samples = len(files)

    # Get reference genome
    #refname = 'no_filter'
    refname = 'autosomal/autosomal'
    fileName = os.path.join(path,'ADNI_GO_2_OmniExpress/',refname+'.frq.count')
    ref_genome = get_ref_genome(fileName)
    p_features = ref_genome.shape[0]

    # Build X matrix
    # TEST_N = n_samples
    TEST_N = 2
    TEST_P = p_features
    X = np.zeros((TEST_N, TEST_P))*np.nan
    for i, f in enumerate(files[:TEST_N]):
        sample = pd.io.parsers.read_csv(f, usecols = ['SNP Name','Allele1 - Forward', 'Allele2 - Forward'], header = 0)
        for j, snp in enumerate(sample['SNP Name'][:TEST_P]):
            try:
                ref = ref_genome.REF[np.argwhere(ref_genome.index == snp)[0][0]] # for the jth snp
                pos_gt = np.argwhere(sample['SNP Name'] == snp)[0][0] # the position for the ith patients of the genotype call for the jth snp
                gt_call = [sample['Allele1 - Forward'][pos_gt], sample['Allele2 - Forward'][pos_gt]]
                gt = to012(gt_call, ref)
                X[i,j] = gt
            except:
                X[i,j] = np.nan
                logging.debug("File {}:".format(f))
                logging.debug("\t\t- SNP {} not found in ref, X[{},{}] = NaN".format(snp, i, j))
        print("sample {}/{} done".format((i,TEST_N)))

    just_names = lambda x: x[-14:-4]
    files_ = map(just_names, files)
    # df = pd.DataFrame(data = X, columns=sample['SNP Name'][:10], index=files_[:3])
    df = pd.DataFrame(data = X, columns=sample['SNP Name'][:TEST_P], index=files_[:TEST_N])

    df.to_csv('n2pALL.csv')

    return df


if __name__ == '__main__':
    out = main()
