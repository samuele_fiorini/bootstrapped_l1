import os
import numpy as np
import pandas as pd

from snp_crawler import get_ref_genome

# Just print the list of the SNPs id after the PLINK filtering step

def main():
    path = '/home/samu/projects/Special_issue_ADNI/data/ALL_PLINK/ADNI_1_PLINK/original_data/test'
    fileName = 'autosomal.frq'
    ref_genome = get_ref_genome(os.path.join(path,fileName))

    SNPs = []
    for snp in ref_genome.index:
        SNPs.append(snp)

    np.savetxt('filtered_snp_list', np.array(SNPs), delimiter=',', fmt='%s')



if __name__ == '__main__':
    main()
