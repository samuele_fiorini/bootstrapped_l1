import os
import numpy as np
import pandas as pd
import pplus
import cPickle as pkl

SIZE_CHUNK = 15

def gt_to_012(gt):
    import numpy as np
    if gt == ['2','2']:
        return 0
    elif gt == ['2','1'] or gt == ['1','2']:
        return 1
    elif gt == ['1','1']:
        return 2
    else:
        return np.nan

def extract_sample_from_line(pc, _line):
    """
    Encode the sample in 012 in parallel
    """
    print 'Starting experiment with id %s' % pc.id
    print 'Master session id %s' % pc.session_id

    line = _line.split(' ') # e.g. ['1', '014_S_0520', '0', '0', '2', '-9', '2', '2', '2', '2', ...]
    _id = line[1]

    _gt_012 = []
    for i in xrange(6,len(line)-1,2):
        gt = [line[i], line[i+1]]
        _gt_012.append(gt_to_012(gt))

    return _id, _gt_012

def get_snp_id(fileName):
    """
    Get the reference genome from plink.frq file
    """
    refdata = pd.io.parsers.read_csv(fileName)

    SNPs = []
    for row in refdata.values:
        splitted_row = np.array(row[0].split(' '))
        pos =  np.argwhere(splitted_row!='') # [CHR SNP A1 A2 C1 C2 G0]
        SNPs.append(splitted_row[pos[1]][0])

    return SNPs

def impute_median(df):
    x = df.as_matrix()
    med = np.nanmedian(x, axis=0)
    for i, col in enumerate(x.T):
        pos = np.argwhere(np.isnan(col))
        if len(pos)>0:
            col[pos] = med[i]
            x[:,i] = col
    return pd.DataFrame(data = x, index = df.index, columns = df.columns)


def main():
    # filename.ped
    path = '/home/samu/projects/Special_issue_ADNI/filtering'
    fileName = 'final.ped'
    num_lines = sum(1 for line in open(os.path.join(path,fileName)))
    print("n = {}".format(num_lines))

    # PPlus Connection instantiation in debug mode
    pc = pplus.PPlusConnection(debug = True)

    # output init
    sample_id = [] # the list of each sample identifier
    sample_gt = [] # a list of lists, each inner list is 1 x p

    with open(os.path.join(path,fileName)) as _file:

        # Read the first SIZE_CHUNK lines
        chunk = [next(_file) for x in xrange(SIZE_CHUNK)]

        condition = True
        lines_counter = SIZE_CHUNK # keeps track of how many chunks were loaded so far
        while condition:
            # submit SIZE_CHUNK jobs
            for _line in chunk:
                pc.submit(extract_sample_from_line, (_line,), depfuncs = (gt_to_012,))

            # Read other SIZE_CHUNK lines
            if lines_counter < num_lines:
                chunk = [next(_file) for x in xrange(min(SIZE_CHUNK, num_lines-lines_counter))] # to avoid overflow
                lines_counter+=min(SIZE_CHUNK, num_lines-lines_counter)
            else:
                condition = False

            # Collect the results from pplus
            results = pc.collect(clean_executed = True)

            for s in results:
                sample_id.append(s[0])
                sample_gt.append(s[1])

            print("**********************************************")
            print("{} samples encoded".format(lines_counter))
            print("{} len(sample_id)".format(len(sample_id)))

        # ------------- save results ------------------------------------------------------- #
        with open('results.pkl', 'w') as dumpfile:
            pkl.dump([sample_id, sample_gt], dumpfile)

        SNP_id = get_snp_id(os.path.join(path,'maf_final.frq'))

        df = pd.DataFrame(data = np.array(sample_gt), index = sample_id, columns = SNP_id)
        # drop columns with few observations
        THRESH = 0.95
        df = df.dropna(thresh = np.round(THRESH*len(sample_id)), axis=1)
        # impute matrix
        #df = df.replace(np.nan, pd.Series(np.nanmedian(df.as_matrix(), axis=1)))
        df2 = impute_median(df)
        df2.to_csv('imputedX.csv')

if __name__ == '__main__':
    main()
