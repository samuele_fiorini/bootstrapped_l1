#!/usr/bin/python
import os
import argparse
import numpy as np
import pandas as pd
import cPickle as pkl

def analyze_pkl(path):
    """
    Get each pkl file and perform analysis.
    """
    sel_feat_dict = dict() # {'snpid': [<BL1L2 score>, <count>]'}
    sel_feat_set = set()
    for _f in os.listdir(path): # browse all pkl files
        if _f.endswith('.pkl'):
            with open(os.path.join(path,_f), 'r') as f:
                res = pkl.load(f)

                # get the stats of that boostrap
                acc =  res['acc_best_model']
                chance =  res['chance']
                if acc > chance: # keep only good models

                    sel_feat = res['selected_features']
                    # iterate on the selected features
                    for feat in sel_feat:
                        if feat not in sel_feat_set:
                            sel_feat_set.add(feat)
                            sel_feat_dict[feat] = [acc - chance, 1]
                        else:
                            sel_feat_dict[feat][0] += acc - chance
                            sel_feat_dict[feat][1] += 1
    return sel_feat_dict

def main(path):
    """
    Perform analysis of BL1 output
    """
    sf = analyze_pkl(path)
    df = pd.DataFrame(data = [sf[k] for k in sf.keys()], index = sf.keys(), columns = ['BL1L2 score', 'counts'])
    df = df.sort_values(by='BL1L2 score', ascending = False)
    df.to_csv(os.path.join(path,'results.csv'))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('output_dir',
                   help='<BL1 output directory>')
    args = parser.parse_args()

    main(args.output_dir)
