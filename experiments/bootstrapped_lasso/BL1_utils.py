from __future__ import print_function
import sys, os
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt

from estimate_number_of_samples import estimate_prob
from estimate_number_of_samples import plot_2D
from estimate_number_of_samples import argmin_fN

import l1l2py
from l1l2signature.utils import RangesScaler

MAX_N = 1500

def get_opt_N(D, d, alpha, T):
    Nrange = np.arange(MAX_N) # the x axis of the plot
    f = estimate_prob(D, d, np.array([alpha]), Nrange)
    opt_N = argmin_fN(Nrange, f, np.array([alpha]), T)
    # plt.savefig(os.path.join('Figures','sampling_probability.png'))
    if np.abs(opt_N - MAX_N) < 10:
        print('Optimal N too large. Used default N = 800')
        opt_N = 800
    elif opt_N == 0:
        print("d = {:.0f} >> D = {}".format(d, D))
        print("Choose a lower d or check the input data.")
        sys.exit(1)
    else:
        print("Estimated optimal N = {}".format(opt_N))
    return opt_N

def train_test_index_split(index, test_size = 0.33):
    """
    Split the input indexes in train and test chunks.
    """
    np.random.shuffle(index)
    ind_tr, ind_ts = index[int(test_size*len(index)):], index[0:int(test_size*len(index))]
    return ind_tr, ind_ts

def get_param_ranges(X, y, tau_scaling_range, get_lambda = False):
    # Get tau range as in L1L2Signature
    rs = RangesScaler(X, y)
    tau_range = rs.tau_range(tau_scaling_range)

    if get_lambda:
        # Get lambda range using the singular values of the Kernel Matrix
        KernelMatrix = np.dot(X,X.T) # linear kernel
    	S = np.linalg.svd(KernelMatrix, compute_uv = 0) # Singular Value Decomposition
    	lambda_max = 2*np.log(np.max(S))/X.shape[0]
    	lambda_min = -5.0 # a small number
        lambda_range = np.logspace(lambda_min, lambda_max, len(tau_range))
        # lambda_range = np.sort(lambda_range)
        return tau_range, lambda_range
    else:
        return tau_range

def save_hist(feat_bootstrap, D):
    plt.subplot(311)
    # h1, bins = np.histogram(feat_bootstrap.ravel())
    # plt.bar(bins[1:], h1, align = 'center')
    h1, bins1, _ = plt.hist(feat_bootstrap.ravel(), bins = D)
    plt.xlabel('feat index')
    plt.ylabel('sampling counts')

    plt.subplot(312)
    _h, bins1, _ = plt.hist(feat_bootstrap.ravel(), bins = D)
    plt.xlim([0,100])
    plt.xlabel('zoom on feat index')
    plt.ylabel('sampling counts')

    plt.subplot(313)
    # h2, bins2 = np.histogram(h1, bins = 20)
    # plt.bar(bins2[1:], h2)
    h2, bins2, _ = plt.hist(h1, bins = 20)
    plt.xlabel('#extractions')
    plt.ylabel('#extracted features')

    plt.suptitle('Random features sampling')
    # plt.show()
    plt.savefig(os.path.join('Figures','feature_bootstrap.png'))

def save_surf(iter, name, surf, xaxis, yaxis):
    """ Draw and save the error surface """
    plt.clf()
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    xx, yy = np.meshgrid(xaxis, yaxis)
    surf = ax.plot_surface(np.log(xx), yy, surf, rstride=1, cstride=1,
                           cmap=cm.coolwarm, linewidth=0, antialiased=False)
    ax.set_title(name)
    ax.set_xlabel(r"log($\tau)$")
    ax.set_ylabel(r"log($\lambda$)")

    # Save the plot
    dirname = 'err_iter_'+str(iter)
    if not os.path.exists(os.path.join('Figures',dirname)):
        os.makedirs(os.path.join('Figures',dirname))
    plt.savefig(os.path.join('Figures', dirname, name))

def print_stats(stats, real_features_names = False, _file = sys.stdout):
    """
    Print some classification report
    """
    print("Chance accuracy = {}".format(stats['chance']), file = _file)
    print("Best   accuracy = {}".format(stats['acc_best_model']), file = _file)
    print("Misclassified training samples = {}".format(stats['miscl_tr_best_model']), file = _file)
    print("Correctly classified training samples = {}".format(stats['okcl_tr_best_model']), file = _file)
    print("Misclassified validation samples = {}\n\n".format(stats['miscl_vld_best_model']), file = _file)
    print("Correctly classified validation samples = {}".format(stats['okcl_vld_best_model']), file = _file)
    print("# Feature selected = {}".format(len(stats['selected_features'])), file = _file)
    if real_features_names:
        print("Feature selected: {}".format(stats['selected_features']), file = _file)
    print("******************************************************* \n \n", file = _file)
