from __future__ import division
import cPickle as pkl
import time, os, sys
import numpy as np
import matplotlib.pyplot as plt

import l1l2py
#from l1l2py.algorithms import l1l2_regularization, ridge_regressioni

# --- CUDA --- #
from l1l2py.algorithms import ridge_regression, l1l2_regularization
from l1l2_pycuda.cu_algorithms import cu_l1l2_regularization
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
# --- CUDA --- #

import BL1_utils as utils
import BL1_genome_tools as gentools


def compute_l1l2(Xtr, ytr, Xvld, yvld, tau_range, lambda_range):
    """
    Compute L1 FS + L2 prediction, return best model.
    """
    # Init error surfaces
    tr_err_surf = np.ones((len(tau_range),len(lambda_range)))
    vld_err_surf = np.ones_like(tr_err_surf)
    acc_surf = np.ones_like(tr_err_surf)
    misclassified_tr_samples = np.zeros_like(tr_err_surf)
    misclassified_vld_samples = np.zeros_like(vld_err_surf)
    # and selected features
    selected_list = []

    # Estimate chance accuracy
    chance = max([np.count_nonzero(ytr == 1), np.count_nonzero(ytr == -1)]) / len(ytr)

    # GPU offload
    n, _ = Xtr.shape
    Xtr32 = Xtr.astype(np.float32)
    Ytr32 = ytr.astype(np.float32)
    gpu_Xtr = pycuda.gpuarray.to_gpu(Xtr32)
    gpu_ytr = pycuda.gpuarray.to_gpu(Ytr32.reshape((n,1)))

    for i, t in enumerate(tau_range):
        print("------------------------------------------------- \n \n")
        print("Running L1 with tau = {} \n\n".format(t))

        # beta_l1 = l1l2_regularization(Xtr, ytr, mu = 0, tau = t, adaptive=False, kmax=10000)
        # try:
        #     beta_l1, niter = cu_l1l2_regularization(gpu_Xtr, gpu_ytr, mu = 0, tau = t,
        #                                             adaptive=False, kmax=10000,
        #                                             return_iterations = True)
        # except ValueError:
        #     sys.exit(-1)
        #     beta_l1, niter = l1l2_regularization(Xtr, ytr, mu = 0, tau = t,
        #                                          adaptive=False, kmax=10000,
        #                                          return_iterations = True)
        beta_l1, niter = cu_l1l2_regularization(gpu_data = gpu_Xtr, gpu_labels = gpu_ytr, mu = 0.0, tau = t,
                                                tolerance = 1e-5,
                                                adaptive=False, kmax=10000,
                                                return_iterations = True)

        if niter > 9999:
            print("\n\n**** L1L2 not converged *****\n\n")

        selected = np.where(beta_l1.ravel() != 0)[0] # how many selected features
        selected_list.append(selected)
        print("\t Features selected: {} \n\n".format(len(selected)))
        if len(selected) > 0 :
            fs_Xtr = Xtr[:,selected]
            fs_Xvld = Xvld[:,selected]
            # Compute L2 solution
            for j, l in enumerate(lambda_range):
                print("\t Running L2 with lambda = {} \n\n".format(l))
                beta_l2 = ridge_regression(fs_Xtr, ytr, mu = l)

                # Estimate training Error
                y_pred_tr = np.dot(fs_Xtr, beta_l2).ravel()
                tr_err = l1l2py.tools.regression_error(ytr, y_pred_tr)
                # Estimate validation error
                y_pred = np.dot(fs_Xvld, beta_l2).ravel()
                vld_err = l1l2py.tools.regression_error(yvld, y_pred)
                acc = 1 - l1l2py.tools.classification_error(yvld, np.sign(y_pred))

                # -------- DEBUG --------------- #
                zero_index = np.where(np.sign(y_pred) == 0)[0]
                if len(zero_index)>0:
                    print("\t!!! {} zeros found in Y !!!!".format(len(zero_index)))
                    print("\tsize of validation set {}".format(len(yvld)))
                    correct = np.where(yvld == np.sign(y_pred))[0]
                    print("\tcorrect answers: {}".format(len(correct)))
                    print("\tstandard acc: {} - corrected acc: {}".format(acc, len(correct) / (len(yvld)-len(zero_index))))
                    print("\t0 prediction for:\nXvld = {}".format(Xvld[zero_index,:]))
                    # sys.exit(-1)
                # -------- DEBUG --------------- #

                # Save errors
                tr_err_surf[i,j] = tr_err
                vld_err_surf[i,j] = vld_err
                acc_surf[i,j] = acc
                misclassified_tr_samples[i,j] = np.sum(ytr != np.sign(y_pred_tr).reshape(ytr.shape))
                misclassified_vld_samples[i,j] = np.sum(yvld != np.sign(y_pred).reshape(yvld.shape))

    # Get the best model
    tau_star_index, lambda_star_index = np.unravel_index(np.argmin(vld_err_surf),
                                                         dims = vld_err_surf.shape)
    tau_star, lambda_star = tau_range[tau_star_index], lambda_range[lambda_star_index]
    selected_features = selected_list[tau_star_index]
    acc_best_model = acc_surf[tau_star_index, lambda_star_index]
    miscl_tr_best_model = misclassified_tr_samples[tau_star_index, lambda_star_index]
    miscl_vld_best_model = misclassified_vld_samples[tau_star_index, lambda_star_index]
    okcl_tr_best_model = len(ytr) - miscl_tr_best_model
    okcl_vld_best_model = len(yvld) - miscl_vld_best_model

    stats = {"tau_star": tau_star, "lambda_star": lambda_star,
             "selected_features": selected_features,
             "acc_best_model": acc_best_model,
             "miscl_tr_best_model": miscl_tr_best_model,
             "miscl_vld_best_model": miscl_vld_best_model,
             "chance": chance,
             "vld_err_surf": vld_err_surf,
             "tr_err_surf": tr_err_surf,
             "acc_surf": acc_surf,
             "okcl_tr_best_model": okcl_tr_best_model,
             "okcl_vld_best_model": okcl_vld_best_model}

    # Print some stats
    utils.print_stats(stats, real_features_names = False)

    return stats


def bootstrap_l1(X, y, opt_N, d, feat_names = [], make_plot = False, gene_table_file = '', SNPs_coord_file = ''):
    """
    Estimate the L1L2 solution on several small portions of the features.
    """
    n, D = X.shape

    # When it's used on snp data it may be useful to bootsrap the same number of snp from each chromosome
    if SNPs_coord_file:
        chr_dict = gentools.SNPs2chr(SNPs_id = feat_names, SNPs_coord_file = SNPs_coord_file)
        feat_bootstrap = gentools.sample_from_chr(chr_dict = chr_dict, size = (opt_N, d), SNPs_id = feat_names)
    else:
        # Repeat bootstrap sampling on the features opt_N times
        feat_bootstrap = np.random.randint(low = 0, high = D, size = (opt_N, d))
        if make_plot: utils.save_hist(feat_bootstrap, D)

    print("All features bootstrap generated")

    selected_features = []
    index = np.arange(n)

    for i in xrange(opt_N):
        print("Split {}/{}".format(i+1,opt_N))
        # Split the dataset
        ind_tr, ind_vld = utils.train_test_index_split(index, test_size = 0.33)

        Xtr = X[ind_tr, :] # select rows
        Xtr = Xtr[:, feat_bootstrap[i,:]] # select columns
        ytr = y[ind_tr]
        Xvld = X[ind_vld, :] # select rows
        Xvld = Xvld[:, feat_bootstrap[i,:]] # select columns
        yvld = y[ind_vld]

        # Get tau and lambda ranges
        tau_range, lambda_range = utils.get_param_ranges(Xtr, ytr,
                                                   tau_scaling_range = l1l2py.tools.geometric_range(1e-3, 0.8, 20),
                                                   get_lambda = True)
        # Estimate l1l2 solution
        # Compute L1 FS
        tic = time.time()

        stats = compute_l1l2(Xtr, ytr, Xvld, yvld, tau_range, lambda_range)
        stats['selected_features'] = feat_names[feat_bootstrap[i,:][stats['selected_features']]]
        selected_features.append(stats['selected_features'])

        tac = time.time()
        print("\t Elapsed time : {} \n\n".format(tac-tic))

        # Save the error surfaces
        if make_plot:
            for name, surf in zip(['Validation Error', 'Accuracy', 'Training Error'], [stats['vld_err_surf'], stats['acc_surf'], stats['tr_err_surf']]):
                utils.save_surf(i, name, surf, tau_range, lambda_range)
        # ---------- ---------- ---------- ---------- ---------- #
        # Write some results
        if not os.path.exists('iters'):
            os.makedirs('iters')

        out_fileName = os.path.join('iters','it_'+str(i))
        with open(out_fileName+'.txt', 'w') as out_file:
            utils.print_stats(stats, real_features_names = True, _file = out_file)

        with open(out_fileName+'.pkl', 'w') as out_file:
            pkl.dump(stats, out_file)
        # ---------- ---------- ---------- ---------- ---------- #

    return selected_features
