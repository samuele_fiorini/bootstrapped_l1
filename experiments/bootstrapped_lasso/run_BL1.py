#!/usr/bin/python
import argparse
import numpy as np
import cPickle as pkl

from l1l2signature.utils import BioDataReader
import l1l2py

import BL1_utils as utils
import BL1_core as core

def main(d, alpha, T, path_to_X, path_to_Y, make_plot, hg_file, SNPs_coord, iter_):
    """
        *** Run bootstrapped L1L2 ***
    """
    # Load dataset
    bd = BioDataReader(data_file = path_to_X,
                       labels_file = path_to_Y,
                       samples_on = 'row')
    n, D = bd.data.shape
    print("Dataset loaded")

    # Get the optimal number of sampling operations
    if iter_ == 'auto':
        opt_N = utils.get_opt_N(D, d, alpha, T)
    else:
        opt_N = int(iter_)
    print("Total number of bootstrap: {}".format(opt_N))

    # Split the dataset (indexes)
    index = np.arange(n)
    ind_tr, ind_ts = utils.train_test_index_split(index, test_size=0.33)

    Xtr = 2**bd.data[ind_tr,:]
    # Xtr = bd.data[ind_tr,:]
    ytr = bd.labels[ind_tr]
    feat_names = bd.variables

    # Perform bootstrapped l1
    sf = core.bootstrap_l1(Xtr, ytr, opt_N, d, feat_names,
                           make_plot = make_plot,
                           gene_table_file = hg_file,
                           SNPs_coord_file = SNPs_coord)

    with open('selected.pkl','w') as f:
        pkl.dump(sf, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--d', metavar='<int>', type=int, default=1E4,
                   help='number of features extracted for each bootstrap')
    parser.add_argument('--alpha', metavar='<int>', type=int, default=10,
                   help='number of times each feature is selected with probability T')
    parser.add_argument('--T', metavar='<float>', type=float, default=0.95,
                   help='probability to select each feature alpha times')
    parser.add_argument('--iter', metavar='{<int> or auto}', type=str, default='auto',
                   help="number of bootstrap iterations, use 'auto' to let the algorithm estimate it")
    parser.add_argument('--make_plot', metavar='True/False', type=bool, default=False,
                  help='Save intermediate plots')
    parser.add_argument('--hg_file', type=argparse.FileType('r'),
                   help='<path to gene table file GRChXX_hgXX.csv>')
    parser.add_argument('--SNPs_coord_file', type=argparse.FileType('r'),
                   help='<path to SNPs position file, i.e. a PLINK.map file plus a header e.g. [chr, snp_id, pos_morgan, bp_coord]>')
    parser.add_argument('X', type=argparse.FileType('r'),
                   help='<path to input matrix X.csv>')
    parser.add_argument('Y', type=argparse.FileType('r'),
                   help='<path to output labels Y.csv>')
    args = parser.parse_args()

    main(args.d, args.alpha, args.T, args.X, args.Y,
         args.make_plot, args.hg_file, args.SNPs_coord_file, args.iter)
