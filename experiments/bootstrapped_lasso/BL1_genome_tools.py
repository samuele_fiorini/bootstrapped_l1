from __future__ import division
import cPickle as pkl
import os, sys
import numpy as np
import pandas as pd

import pplus

def make_bootstrap(pc, chr_dict, SNPs_id, chr_d):
    """
    Generate a chromosome-guided bootstrap
    """
    bootstrap = []
    for chr_name in chr_dict.keys():
        chr_snps = chr_dict[chr_name] # get the values
        ch_index = int(chr_name[3:]) - 1 # chr_d index (index of how many snps to generate for each chr)

        if  chr_d[ch_index]> 0:
            local_index = np.random.randint(low = 0, high = len(chr_snps), size = (chr_d[ch_index],))

            snpid = [] # the list of all snps id
            for ind in local_index:
                snpid.append(chr_snps[ind][0])

            global_index = [np.where(SNPs_id == k)[0].tolist()[0] for k in snpid] # this contains the global position
            bootstrap.extend(global_index)

    return bootstrap


def get_snp_list(chr_id, chr_chunk, SNPs_set):
    """
    Get the SNPs of each chromosome.
    """
    snp_list = []
    for snp_id, bp_pos in zip(chr_chunk['snp_id'], chr_chunk['bp_coord']):

        if snp_id in SNPs_set:
            snp_list.append((snp_id, bp_pos))
    return snp_list


def SNPs2chr(SNPs_id = [], SNPs_coord_file = ''):
    """
    Return a dictionary where each key is a chromosome 'chrXX' and its value is a list of bp position of its SNPs.
    SNPs_id is a list containing the name of the SNPs to analyze.

    e.g. chr_dict = {'chr1': [(snp1, 9343), (snp2, 3423), ...]}
    """

    plink_map = pd.io.parsers.read_csv(SNPs_coord_file, sep = '\t')
    print('SNPs coordinate loaded')

    # distribute the counts
    SNPs_set = frozenset(SNPs_id)
    chr_dict = dict()

    for chr_id in xrange(1,23):
        chr_chunk = plink_map.iloc[plink_map['chr'].values == chr_id,:] # select its snps
        snp_list = get_snp_list(chr_id, chr_chunk, SNPs_set)
        chr_dict['chr'+str(chr_id)] = snp_list

    return chr_dict


def sample_from_chr(chr_dict, size, SNPs_id):
    """
    Generate the feature bootsrap equally distributed in each chromosome.
    """
    # # --- DEBUG --- #
    # for k in chr_dict.keys():
    #     if len(chr_dict[k]) > 0:
    #         print k
    #         print chr_dict[k][:3]
    # # --- DEBUG --- #

    n, d = size[0], size[1] # number of bootsrap | dimension of each bootstrap

    # init output matrix
    snp_bootstrap = np.zeros(size)

    # count how many snp is mapped to each chromosome
    how_many_SNPs_each_chr = np.zeros(len(chr_dict))
    for k in chr_dict.keys():
        ch_index = int(k[3:]) - 1 # chr_d index (index of how many snps to generate for each chr)
        how_many_SNPs_each_chr[ch_index] = len(chr_dict[k])

    D = np.sum(how_many_SNPs_each_chr) # total number of snps (it's just the dimensionality of the problem)
    frac = np.array(how_many_SNPs_each_chr) / D # the fraction of the snp that should be estracted from each chromosome

    chr_d = np.floor(d * frac) # how many snp must be extracted from each chr
    residuals = d - np.sum(chr_d) # if there are any residuals (i.e. not assigned to any chr)
    chr_d[np.argmax(chr_d)]+= residuals # add them to the most populated chr

    # PPlus Connection instantiation in debug mode
    pc = pplus.PPlusConnection(debug = True)

    # Generate the snps bootstrap
    for i in xrange(n):
        pc.submit(make_bootstrap, (chr_dict, SNPs_id, chr_d,))
        print("Bootstrap {} submitted".format(i))

    # collect results
    res = pc.collect(clean_executed = True)

    for i, bootstrap in enumerate(res):
        snp_bootstrap[i,:] = bootstrap
        print("Bootstrap {} collected".format(i))

    return snp_bootstrap.astype(int)
