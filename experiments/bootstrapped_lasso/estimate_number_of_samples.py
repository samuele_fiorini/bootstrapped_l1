from __future__ import division
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

from scipy.stats import binom

def estimate_prob(D, d, alpha, N):
    """
    P[selecting each feature at least *alpha* times performing
    N* sampling operations of *d* features from the *D* at hand]
    f(alpha,N) = 1 - sum_{i=0}^alpha bin(N i)p^i(1-p)^{N-i}
    """
    _p = d/D # P[select one of the D features sampling d ones]
    f_N = np.zeros((len(alpha),len(N)))
    for i, _alpha in enumerate(alpha):
        for j, _n in enumerate(N):
            acc = 0 # accumulator of the sum in f(N)
            for a in xrange(_alpha+1):
                acc += binom.pmf(n = _n, k = a, p = _p)
                f_N[i,j] = 1 - acc
    return f_N

def argmin_fN(N, f_N, alpha, T):
    f_N = f_N[0,:]
    _argmin = np.argmin(np.abs(f_N - T))
    return _argmin

def plot_2D(N, f_N, alpha, T):
    # Select the first dimension
    f_N = f_N[0,:]
    # find where it is more similar to T
    _argmin = np.argmin(np.abs(f_N - T))

    # --- 2D PLOT --- #
    plt.plot(N, f_N, label = 'f(N)')
    plt.hlines(y = T, xmin = 0, xmax = _argmin,
               color = 'r', linewidth = '2', linestyle = '--', label = 'threshold')
    plt.axvline(x = _argmin, ymin = 0, ymax = f_N[_argmin],
                color = 'g', linewidth = '2', linestyle = '--', label = '#sampling')
    plt.xlabel("number of extraction (N)")
    plt.ylabel("probability")
    plt.title(r"P[selecting each feature at least $\alpha$ = {} times]".format(alpha[0]))
    plt.legend(loc='best', ncol=1, fancybox=True, shadow=True)
    plt.grid()
    return _argmin

def surf_3D(N, f, alpha, T):
    xx, yy = np.meshgrid(N, alpha)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(xx, yy, f, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
    ax.set_xlabel('N')
    ax.set_ylabel(r"$\alpha$")
    ax.set_title(r"P[selecting each feature $\alpha$ times after N sampling ops]")

    surf2 = ax.plot_wireframe(xx, yy, T*np.ones_like(xx),
            rstride=10, cstride=10, alpha = 0.5)

def main():
    # --- Problem dimensions
    D = 451349 # num of features
    # D = 50000
    d = 10000  # dim of sampling

    # -------------------- 2D analysis -------------------- #
    # --- Fixed values
    alpha = np.array([10]) # number of times each feature is selected
    T = 0.95   # P[each feature is selected at least alpha times]

    # --- independent variable -> number of sampling operations
    N = np.arange(1500) # the x axis of the plot
    f_N = estimate_prob(D, d, alpha, N)

    _argmin = plot_2D(N, f_N, alpha, T)
    print("{} sampling operations needed".format(N[_argmin]))
    plt.show()

if __name__ == '__main__':
    main()










    # # -------------------- 3D analysis -------------------- #
    # # --- Fixed values
    # alpha = np.arange(1,21) # number of times each feature is selected
    # T = 0.95   # P[each feature is selected at least alpha times]
    #
    # # --- independent variable -> number of sampling operations
    # N = np.arange(1000) # the x axis of the plot
    # f = estimate_prob(D, d, alpha, N)
    #
    # surf_3D(N, f, alpha, T)
    # plt.show(block = False)
