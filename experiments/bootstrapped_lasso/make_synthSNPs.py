#!/usr/bin/python
from __future__ import division
import argparse
import os
import numpy as np
import pandas as pd

MAX_ABS_BETA = 3

def generate_beta(d):
    """
    Generate d nonzero beta
    """
    pos_beta = np.random.randint(low = 1, high = MAX_ABS_BETA+1, size = (d//3,))
    neg_beta = -np.random.randint(low = 1, high = MAX_ABS_BETA+1, size = (d-len(pos_beta),))
    _beta = np.hstack((pos_beta,neg_beta))
    np.random.shuffle(_beta)
    return _beta

def main(n, D, relevant, name):
    """
    Generate a synthetic SNP dataset compatible with the L1L2Signature format
    """
    X = np.random.randint(3, size = (n, D))
    beta = generate_beta(relevant)
    Y = np.sign(np.dot(X[:,:relevant], beta) + 0.1*np.random.randn(n,))

    samples_id = ['sample_'+str(i) for i in range(n)]
    features_id = ['snp_'+str(i) for i in range(D)]

    dataX = pd.DataFrame(data = X, index = samples_id, columns = features_id)
    dataY = pd.DataFrame(data = Y, index = samples_id, columns = ['Class'])

    # Save the results
    if not os.path.exists(name):
        os.makedirs(name)

    dataX.to_csv(os.path.join(name,'X.csv'))
    dataY.to_csv(os.path.join(name,'Y.csv'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n', metavar='n', type=int, default=300,
                   help='number of patients to generate')
    parser.add_argument('--D', metavar='D', type=int, default=50000,
                   help='total number of SNPs')
    parser.add_argument('--relevant', metavar='relevant', type=int, default=5,
                   help='number of relevant features (it must be > D)')
    parser.add_argument('--out', metavar='out', type=str, default='synthData',
                  help='name of the synthetic dataset')
    args = parser.parse_args()

    main(args.n, args.D, args.relevant, args.out)
